/**
 * Connector for retrieving OpenID Connect claims from an HTTP endpoint.
 */
package com.nimbusds.openid.connect.provider.spi.claims.http;