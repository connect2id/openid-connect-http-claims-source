package com.nimbusds.openid.connect.provider.spi.claims.http;

import com.nimbusds.oauth2.sdk.id.Identifier;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectAuthentication;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import net.minidev.json.JSONObject;

/**
 * JSON helper methods.
 */
public class JSONUtils {


        /**
         * Returns a JSON object representation of the specified subject
         * session.
         *
         * @param subjectSession The subject session.
         *
         * @return The JSON object.
         */
        public static JSONObject toJSONObject(final SubjectSession subjectSession) {

                // Format must comply with https://connect2id.com/products/server/docs/integration/session-store#subject-session

                var o = new JSONObject();
                o.put("sub", subjectSession.getSubject().getValue());

                SubjectAuthentication subjectAuth = subjectSession.getSubjectAuthentication();
                o.put("auth_time", subjectAuth.getTime().getEpochSecond());
                if (subjectAuth.getACR() != null) {
                        o.put("acr", subjectAuth.getACR().getValue());
                }
                if (subjectAuth.getAMRList() != null) {
                        o.put("amr", Identifier.toStringList(subjectAuth.getAMRList()));
                }

                o.put("creation_time", subjectSession.getCreationTime().getEpochSecond());

                o.put("max_life", subjectSession.getMaxLifetime());
                o.put("auth_life", subjectSession.getAuthLifetime());
                o.put("max_idle", subjectSession.getMaxIdleTime());

                o.put("rps", Identifier.toStringList(subjectSession.getRelyingParties()));

                if (subjectSession.getClaims() != null) {
                        o.put("claims", subjectSession.getClaims());
                }

                if (subjectSession.getData() != null) {
                        o.put("data", subjectSession.getData());
                }

                return o;
        }


        private JSONUtils() {
        }
}
