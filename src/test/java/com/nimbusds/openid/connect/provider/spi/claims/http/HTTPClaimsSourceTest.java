package com.nimbusds.openid.connect.provider.spi.claims.http;


import com.nimbusds.jwt.util.DateUtils;
import com.nimbusds.langtag.LangTag;
import com.nimbusds.oauth2.sdk.ParseException;
import com.nimbusds.oauth2.sdk.Scope;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Issuer;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.oauth2.sdk.util.JSONObjectUtils;
import com.nimbusds.oauth2.sdk.util.date.DateWithTimeZoneOffset;
import com.nimbusds.openid.connect.provider.spi.claims.ClaimsSourceRequestContext;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectAuthentication;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSessionID;
import com.nimbusds.openid.connect.sdk.assurance.IdentityTrustFramework;
import com.nimbusds.openid.connect.sdk.assurance.IdentityVerification;
import com.nimbusds.openid.connect.sdk.assurance.VerificationProcess;
import com.nimbusds.openid.connect.sdk.assurance.claims.VerifiedClaimsSet;
import com.nimbusds.openid.connect.sdk.assurance.evidences.QESEvidence;
import com.nimbusds.openid.connect.sdk.claims.*;
import com.nimbusds.openid.connect.sdk.rp.OIDCClientInformation;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static net.jadler.Jadler.*;
import static org.junit.Assert.*;


public class HTTPClaimsSourceTest {
	
	
	public static final Issuer ISSUER = new Issuer("https://c2id.com");

	private static final Subject SUBJECT = new Subject("alice");

	private static final String EMAIL = "alice@wonderland.net";

	private static final Instant ONE_HOUR_AGO = DateUtils.nowWithSecondsPrecision().toInstant().minus(1, ChronoUnit.HOURS);

	private static final Instant ONE_DAY_AGO = DateUtils.nowWithSecondsPrecision().toInstant().minus(1, ChronoUnit.DAYS);

	private static final Set<ClientID> RELYING_PARTIES = Set.of(new ClientID("123"), new ClientID("456"));

	private static final SubjectSessionID SUBJECT_SESSION_ID = () -> "WYqFXK7Q4HFnJv0hiT3Fgw.-oVkvSXgalUuMQDfEsh1lw";

	private static final SubjectSession MINIMAL_SUBJECT_SESSION = new SubjectSession() {
		@Override
		public Subject getSubject() {
			return SUBJECT;
		}

		@Override
		public SubjectAuthentication getSubjectAuthentication() {
			return new SubjectAuthentication() {
				@Override
				public Subject getSubject() {
					return SUBJECT;
				}

				@Override
				public Instant getTime() {
					return ONE_HOUR_AGO;
				}

				@Override
				public @Nullable ACR getACR() {
					return null;
				}

				@Override
				public @Nullable List<AMR> getAMRList() {
					return null;
				}
			};
		}

		@Override
		public Instant getCreationTime() {
			return ONE_DAY_AGO;
		}

		@Override
		public long getMaxLifetime() {
			return 60 * 24 * 30;
		}

		@Override
		public long getAuthLifetime() {
			return 60 * 24 * 7;
		}

		@Override
		public long getMaxIdleTime() {
			return 60 * 24 * 3;
		}

		@Override
		public Set<ClientID> getRelyingParties() {
			return RELYING_PARTIES;
		}

		@Override
		public @Nullable JSONObject getClaims() {
			return null;
		}

		@Override
		public @Nullable JSONObject getData() {
			return null;
		}
	};

	private static final Scope CONSENTED_SCOPE = new Scope("openid", "email");
	
	
	static class JSONObjectMatcher extends BaseMatcher<String> {
		
		private final Issuer expectedIssuer;
		
		private final Subject expectedSubject;
		
		private final Set<String> expectedClaimNames;
		
		private final @Nullable List<LangTag> expectedLangTags;
		
		private final @Nullable JSONObject expectedClaimsData;
		
		private final @Nullable ClaimsTransport expectedClaimsTransport;

		private final @Nullable SubjectSessionID expectedSubjectSessionID;
		
		private final @Nullable SubjectSession expectedSubjectSession;

		private final @Nullable Scope expectedScope;
		
		
		public JSONObjectMatcher(Issuer expectedIssuer,
					 Subject expectedSubject,
					 Set<String> expectedClaimNames,
					 @Nullable List<LangTag> expectedLangTags,
					 @Nullable JSONObject expectedClaimsData,
					 @Nullable ClaimsTransport expectedClaimsTransport,
					 @Nullable SubjectSessionID expectedSubjectSessionID,
					 @Nullable SubjectSession expectedSubjectSession,
					 @Nullable Scope expectedScope) {
			assertNotNull(expectedIssuer);
			this.expectedIssuer = expectedIssuer;
			assertNotNull(expectedSubject);
			this.expectedSubject = expectedSubject;
			assertNotNull(expectedClaimNames);
			this.expectedClaimNames = expectedClaimNames;
			this.expectedLangTags = expectedLangTags;
			this.expectedClaimsData = expectedClaimsData;
			this.expectedClaimsTransport = expectedClaimsTransport;
			this.expectedSubjectSessionID = expectedSubjectSessionID;
			this.expectedSubjectSession = expectedSubjectSession;
			this.expectedScope = expectedScope;
		}
		
		
		@Override
		public boolean matches(Object o) {
			
			try {
				JSONObject requestJSONObject = JSONObjectUtils.parse(o.toString());

				System.out.println("Request JSON object:\n" + requestJSONObject);
				
				if (! expectedIssuer.getValue().equals(requestJSONObject.get("iss"))) {
					return false;
				}
				
				if (! expectedSubject.getValue().equals(requestJSONObject.get("sub"))) {
					return false;
				}
				
				Set<String> requestedClaims = new HashSet<>(JSONObjectUtils.getStringList(requestJSONObject, "claims"));
				
				if (! expectedClaimNames.equals(requestedClaims)) {
					return false;
				}
				
				if (expectedClaimsData == null && requestJSONObject.get("claims_data") != null) {
					return false;
				} else if (expectedClaimsData != null && ! expectedClaimsData.equals(JSONObjectUtils.getJSONObject(requestJSONObject, "claims_data"))) {
					return false;
				}

				if (expectedClaimsTransport == null && requestJSONObject.containsKey("claims_transport")) {
					return false;
				}else if (expectedClaimsTransport != null && ! expectedClaimsTransport.name().toLowerCase().equals(requestJSONObject.get("claims_transport"))) {
					return false;
				}

				if (expectedSubjectSessionID == null && requestJSONObject.get("sub_sid") != null) {
					return false;
				} else if (expectedSubjectSessionID != null && ! expectedSubjectSessionID.getValue().equals(JSONObjectUtils.getString(requestJSONObject, "sub_sid"))) {
					return false;
				}

				if (expectedSubjectSession == null && requestJSONObject.get("sub_session") != null) {
					return false;
				} else if (expectedSubjectSession != null && ! JSONUtils.toJSONObject(expectedSubjectSession).equals(JSONObjectUtils.getJSONObject(requestJSONObject, "sub_session"))) {
					return false;
				}

				if (expectedScope == null && requestJSONObject.get("scope") != null) {
					return false;
				} else if (expectedScope != null && ! expectedScope.toStringList().equals(JSONObjectUtils.getStringList(requestJSONObject, "scope"))) {
					return false;
				}

				return true;

			} catch (ParseException e) {
				System.err.println(e.getMessage());
				throw new RuntimeException(e.getMessage());
			}
		}
		
		
		@Override
		public void describeTo(Description description) {
			
		}
	}
	
	
	@Before
	public void setUp() {
		initJadler();
	}


	@After
	public void tearDown() {
		closeJadler();
		System.clearProperty("op.httpClaimsSource.url");
		System.clearProperty("op.httpClaimsSource.includeSubjectSessionID");
		System.clearProperty("op.httpClaimsSource.includeSubjectSession");
	}
	
	
	private HTTPClaimsSource getConfiguredConnector()
		throws Exception {
		
		System.setProperty("op.httpClaimsSource.url", "http://localhost:" + port() + "/claims-source/");
		
		HTTPClaimsSource claimsSource = new HTTPClaimsSource();
		claimsSource.init(new MockServletInitContext());
		
		System.out.println("Test claims source URL: " + claimsSource.getConfiguration().url);
		
		return claimsSource;
	}
	
	
	@Test
	public void testSimpleClaimsRetrieval_userInfoTransport()
		throws Exception {
		
		Set<String> requestedClaims = Set.of(
			"email",
			"email_verified",
			"name",
			"given_name",
			"family_name"
			);
		
		var resultToReturn = new UserInfo(SUBJECT);
		resultToReturn.setEmailAddress(EMAIL);
		resultToReturn.setEmailVerified(true);
		resultToReturn.setName("Alice Adams");
		resultToReturn.setGivenName("Alice");
		resultToReturn.setFamilyName("Adams");
		
		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				null,
				ClaimsTransport.USERINFO,
				null,
				null, null))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");
		
		HTTPClaimsSource claimsSource = getConfiguredConnector();
		
		assertTrue(claimsSource.isEnabled());
		
		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}

				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}


				@Override
				public String getClientIPAddress() {
					return null;
				}
				
				
				@Override
				public AccessToken getUserInfoAccessToken() {
					return new BearerAccessToken(); // simulate
				}
				
				
				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}
				
				
				@Override
				public ClaimsTransport getClaimsTransport() {
					return ClaimsTransport.USERINFO;
				}
				
				
				@Override
				public JSONObject getClaimsData() {
					return null;
				}
			});
		
		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}
	
	
	@Test
	public void testSimpleClaimsRetrieval_idTokenTransport()
		throws Exception {
		
		Set<String> requestedClaims = Set.of(
			"email",
			"email_verified",
			"name",
			"given_name",
			"family_name"
			);
		
		var resultToReturn = new UserInfo(SUBJECT);
		resultToReturn.setEmailAddress(EMAIL);
		resultToReturn.setEmailVerified(true);
		resultToReturn.setName("Alice Adams");
		resultToReturn.setGivenName("Alice");
		resultToReturn.setFamilyName("Adams");
		
		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				null,
				ClaimsTransport.ID_TOKEN,
				null,
				null, null))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");
		
		HTTPClaimsSource claimsSource = getConfiguredConnector();
		
		assertTrue(claimsSource.isEnabled());
		
		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}


				@Override
				public String getClientIPAddress() {
					return null;
				}
				
				
				@Override
				public AccessToken getUserInfoAccessToken() {
					return null;
				}
				
				
				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}
				
				
				@Override
				public ClaimsTransport getClaimsTransport() {
					return ClaimsTransport.ID_TOKEN;
				}
				
				
				@Override
				public JSONObject getClaimsData() {
					return null;
				}
			});
		
		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}
	
	
	@Test
	public void testSimpleClaimsRetrieval_unknownTransport()
		throws Exception {
		
		Set<String> requestedClaims = Set.of(
			"email",
			"email_verified",
			"name",
			"given_name",
			"family_name"
			);
		
		var resultToReturn = new UserInfo(SUBJECT);
		resultToReturn.setEmailAddress(EMAIL);
		resultToReturn.setEmailVerified(true);
		resultToReturn.setName("Alice Adams");
		resultToReturn.setGivenName("Alice");
		resultToReturn.setFamilyName("Adams");
		
		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				null,
				null,  // transport not known / applicable
				null,
				null, null))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");
		
		HTTPClaimsSource claimsSource = getConfiguredConnector();
		
		assertTrue(claimsSource.isEnabled());
		
		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}
				
				
				@Override
				public String getClientIPAddress() {
					return null;
				}
				
				
				@Override
				public AccessToken getUserInfoAccessToken() {
					return null;
				}
				
				
				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}
				
				
				@Override
				public ClaimsTransport getClaimsTransport() {
					return null; // not applicable
				}
				
				
				@Override
				public JSONObject getClaimsData() {
					return null;
				}
			});
		
		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}
	
	
	@Test
	public void testWithClaimsData()
		throws Exception {
		
		Set<String> requestedClaims = Set.of(
			"verified:email",
			"verified:name"
		);
		
		var resultToReturn = new UserInfo(SUBJECT);
		var claims = new PersonClaims();
		claims.setEmailAddress(EMAIL);
		claims.setName("Alice Adams");
		var verification = new IdentityVerification(
			IdentityTrustFramework.EIDAS_IAL_HIGH,
			new DateWithTimeZoneOffset(new Date()),
			new VerificationProcess("801d98c1-280e-4f38-a0c5-7d6427a70e90"),
			new QESEvidence(
				new Issuer("https://eidas.c2id.com"),
				"b9f6425d-1e03-45bd-a976-a9261ab33435",
				new DateWithTimeZoneOffset(new Date())
			));
		var verifiedClaimsSet = new VerifiedClaimsSet(verification, claims);
		resultToReturn.setVerifiedClaims(verifiedClaimsSet);
		
		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				verification.toJSONObject(),
				ClaimsTransport.USERINFO,
				null,
				null, null))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");
		
		HTTPClaimsSource claimsSource = getConfiguredConnector();
		
		assertTrue(claimsSource.isEnabled());
		
		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}

				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}

				@Override
				public String getClientIPAddress() {
					return null;
				}
				
				
				@Override
				public AccessToken getUserInfoAccessToken() {
					return new BearerAccessToken(); // simulate
				}
				
				
				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}
				
				
				@Override
				public ClaimsTransport getClaimsTransport() {
					return ClaimsTransport.USERINFO;
				}
				
				
				@Override
				public JSONObject getClaimsData() {
					return verification.toJSONObject();
				}
			});
		
		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}
	
	
	@Test
	public void testInvalidSubjectReturnsEmptyJSONObject()
		throws Exception {
		
		Set<String> requestedClaims = Set.of(
			"email",
			"name"
			);
		
		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.respond()
			.withStatus(200)
			.withBody("{}")
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");
		
		HTTPClaimsSource claimsSource = getConfiguredConnector();
		
		assertTrue(claimsSource.isEnabled());
		
		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}


				@Override
				public String getClientIPAddress() {
					return null;
				}
				
				
				@Override
				public AccessToken getUserInfoAccessToken() {
					return new BearerAccessToken(); // simulate
				}
				
				
				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}
				
				
				@Override
				public ClaimsTransport getClaimsTransport() {
					return null;
				}
				
				
				@Override
				public JSONObject getClaimsData() {
					return null;
				}
			});
		
		assertNull(result);
	}


	@Test
	public void testSimpleClaimsRetrieval_withSubjectSessionID_enabled()
		throws Exception {

		Set<String> requestedClaims = Set.of(
			"email",
			"email_verified"
		);

		var resultToReturn = new UserInfo(SUBJECT);
		resultToReturn.setEmailAddress(EMAIL);
		resultToReturn.setEmailVerified(true);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				null,
				ClaimsTransport.ID_TOKEN,
				SUBJECT_SESSION_ID,
				null, null))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");

		System.setProperty("op.httpClaimsSource.includeSubjectSessionID", "true");

		HTTPClaimsSource claimsSource = getConfiguredConnector();

		assertTrue(claimsSource.isEnabled());

		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}


				@Override
				public String getClientIPAddress() {
					return null;
				}


				@Override
				public AccessToken getUserInfoAccessToken() {
					return null;
				}


				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}


				@Override
				public ClaimsTransport getClaimsTransport() {
					return ClaimsTransport.ID_TOKEN;
				}


				@Override
				public JSONObject getClaimsData() {
					return null;
				}


				@Override
				public SubjectSessionID getSubjectSessionID() {
					return SUBJECT_SESSION_ID;
				}


				@Override
				public SubjectSession getSubjectSession() {
					return MINIMAL_SUBJECT_SESSION;
				}
			});

		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}


	@Test
	public void testSimpleClaimsRetrieval_withSubjectSessionID_disabled()
		throws Exception {

		Set<String> requestedClaims = Set.of(
			"email",
			"email_verified"
		);

		var resultToReturn = new UserInfo(SUBJECT);
		resultToReturn.setEmailAddress(EMAIL);
		resultToReturn.setEmailVerified(true);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				null,
				ClaimsTransport.ID_TOKEN,
				null,
				null, null))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");

		System.setProperty("op.httpClaimsSource.includeSubjectSessionID", "false");

		HTTPClaimsSource claimsSource = getConfiguredConnector();

		assertTrue(claimsSource.isEnabled());

		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}


				@Override
				public String getClientIPAddress() {
					return null;
				}


				@Override
				public AccessToken getUserInfoAccessToken() {
					return null;
				}


				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}


				@Override
				public ClaimsTransport getClaimsTransport() {
					return ClaimsTransport.ID_TOKEN;
				}


				@Override
				public JSONObject getClaimsData() {
					return null;
				}


				@Override
				public SubjectSessionID getSubjectSessionID() {
					return SUBJECT_SESSION_ID;
				}


				@Override
				public SubjectSession getSubjectSession() {
					return MINIMAL_SUBJECT_SESSION;
				}
			});

		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}


	@Test
	public void testSimpleClaimsRetrieval_withSubjectSession_enabled()
		throws Exception {

		Set<String> requestedClaims = Set.of(
			"email",
			"email_verified"
		);

		var resultToReturn = new UserInfo(SUBJECT);
		resultToReturn.setEmailAddress(EMAIL);
		resultToReturn.setEmailVerified(true);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				null,
				ClaimsTransport.ID_TOKEN,
				null,
				MINIMAL_SUBJECT_SESSION, null))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");

		System.setProperty("op.httpClaimsSource.includeSubjectSession", "true");

		HTTPClaimsSource claimsSource = getConfiguredConnector();

		assertTrue(claimsSource.isEnabled());

		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}


				@Override
				public String getClientIPAddress() {
					return null;
				}


				@Override
				public AccessToken getUserInfoAccessToken() {
					return null;
				}


				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}


				@Override
				public ClaimsTransport getClaimsTransport() {
					return ClaimsTransport.ID_TOKEN;
				}


				@Override
				public JSONObject getClaimsData() {
					return null;
				}


				@Override
				public SubjectSessionID getSubjectSessionID() {
					return SUBJECT_SESSION_ID;
				}

				@Override
				public SubjectSession getSubjectSession() {
					return MINIMAL_SUBJECT_SESSION;
				}
			});

		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}


	@Test
	public void testSimpleClaimsRetrieval_withSubjectSession_disabled()
		throws Exception {

		Set<String> requestedClaims = Set.of(
			"email",
			"email_verified"
		);

		var resultToReturn = new UserInfo(SUBJECT);
		resultToReturn.setEmailAddress(EMAIL);
		resultToReturn.setEmailVerified(true);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				null,
				ClaimsTransport.ID_TOKEN,
				null,
				null, null))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");

		System.setProperty("op.httpClaimsSource.includeSubjectSession", "false");

		HTTPClaimsSource claimsSource = getConfiguredConnector();

		assertTrue(claimsSource.isEnabled());

		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}


				@Override
				public String getClientIPAddress() {
					return null;
				}


				@Override
				public AccessToken getUserInfoAccessToken() {
					return null;
				}


				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}


				@Override
				public ClaimsTransport getClaimsTransport() {
					return ClaimsTransport.ID_TOKEN;
				}


				@Override
				public JSONObject getClaimsData() {
					return null;
				}


				@Override
				public SubjectSessionID getSubjectSessionID() {
					return SUBJECT_SESSION_ID;
				}


				@Override
				public SubjectSession getSubjectSession() {
					return MINIMAL_SUBJECT_SESSION;
				}
			});

		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}


	@Test
	public void testSimpleClaimsRetrieval_withScope_enabled()
		throws Exception {

		Set<String> requestedClaims = Set.of(
			"email",
			"email_verified"
		);

		var resultToReturn = new UserInfo(SUBJECT);
		resultToReturn.setEmailAddress(EMAIL);
		resultToReturn.setEmailVerified(true);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				null,
				ClaimsTransport.ID_TOKEN,
				null,
				null,
				CONSENTED_SCOPE))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");

		System.setProperty("op.httpClaimsSource.includeScope", "true");

		HTTPClaimsSource claimsSource = getConfiguredConnector();

		assertTrue(claimsSource.isEnabled());

		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}


				@Override
				public String getClientIPAddress() {
					return null;
				}


				@Override
				public AccessToken getUserInfoAccessToken() {
					return null;
				}


				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}


				@Override
				public ClaimsTransport getClaimsTransport() {
					return ClaimsTransport.ID_TOKEN;
				}


				@Override
				public JSONObject getClaimsData() {
					return null;
				}


				@Override
				public SubjectSessionID getSubjectSessionID() {
					return SUBJECT_SESSION_ID;
				}

				@Override
				public SubjectSession getSubjectSession() {
					return MINIMAL_SUBJECT_SESSION;
				}

				@Override
				public Scope getScope() {
					return CONSENTED_SCOPE;
				}
			});

		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}


	@Test
	public void testSimpleClaimsRetrieval_withScope_disabled()
		throws Exception {

		Set<String> requestedClaims = Set.of(
			"email",
			"email_verified"
		);

		var resultToReturn = new UserInfo(SUBJECT);
		resultToReturn.setEmailAddress(EMAIL);
		resultToReturn.setEmailVerified(true);

		onRequest()
			.havingMethodEqualTo("POST")
			.havingHeaderEqualTo("Accept", "application/json")
			.havingPathEqualTo("/claims-source/")
			.havingBody(new JSONObjectMatcher(
				ISSUER,
				SUBJECT,
				requestedClaims,
				null,
				null,
				ClaimsTransport.ID_TOKEN,
				null,
				null,
				null))
			.respond()
			.withStatus(200)
			.withBody(resultToReturn.toJSONString())
			.withEncoding(StandardCharsets.UTF_8)
			.withContentType("application/json; charset=UTF-8");

		System.setProperty("op.httpClaimsSource.includeScope", "false");

		HTTPClaimsSource claimsSource = getConfiguredConnector();

		assertTrue(claimsSource.isEnabled());

		UserInfo result = claimsSource.getClaims(
			SUBJECT,
			requestedClaims,
			null,
			new ClaimsSourceRequestContext() {
				@Override
				public ClientID getClientID() {
					return new ClientID("123"); // simulate
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation() {
					return null;
				}


				@Override
				public @Nullable OIDCClientInformation getOIDCClientInformation(final ClientID clientID) {
					return null;
				}


				@Override
				public String getClientIPAddress() {
					return null;
				}


				@Override
				public AccessToken getUserInfoAccessToken() {
					return null;
				}


				@Override
				public Issuer getIssuer() {
					return ISSUER;
				}


				@Override
				public ClaimsTransport getClaimsTransport() {
					return ClaimsTransport.ID_TOKEN;
				}


				@Override
				public JSONObject getClaimsData() {
					return null;
				}


				@Override
				public SubjectSessionID getSubjectSessionID() {
					return SUBJECT_SESSION_ID;
				}


				@Override
				public SubjectSession getSubjectSession() {
					return MINIMAL_SUBJECT_SESSION;
				}

				@Override
				public Scope getScope() {
					return CONSENTED_SCOPE;
				}
			});

		assertEquals(resultToReturn.toJSONObject().toJSONString(), result.toJSONObject().toJSONString());
	}
}
