package com.nimbusds.openid.connect.provider.spi.claims.http;

import com.nimbusds.jwt.util.DateUtils;
import com.nimbusds.oauth2.sdk.id.ClientID;
import com.nimbusds.oauth2.sdk.id.Identifier;
import com.nimbusds.oauth2.sdk.id.Subject;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectAuthentication;
import com.nimbusds.openid.connect.provider.spi.internal.sessionstore.SubjectSession;
import com.nimbusds.openid.connect.sdk.claims.ACR;
import com.nimbusds.openid.connect.sdk.claims.AMR;
import net.minidev.json.JSONObject;
import org.checkerframework.checker.nullness.qual.Nullable;
import org.junit.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class JSONUtilsTest {


        private static final Subject SUBJECT = new Subject("alice");

        private static final Instant ONE_HOUR_AGO = DateUtils.nowWithSecondsPrecision().toInstant().minus(1, ChronoUnit.HOURS);

        private static final Instant ONE_DAY_AGO = DateUtils.nowWithSecondsPrecision().toInstant().minus(1, ChronoUnit.DAYS);

        private static final ACR ACR = new ACR("https://loa.c2id.com/high");

        private static final List<AMR> AMR_LIST = List.of(AMR.MFA, AMR.FACE, AMR.PIN);

        private static final Set<ClientID> RELYING_PARTIES = Set.of(new ClientID("123"), new ClientID("456"));

        private static final JSONObject CLAIMS;

        private static final JSONObject DATA;

        static {
                CLAIMS = new JSONObject();
                CLAIMS.put("name", "Alice Adams");
                CLAIMS.put("age", 22);

                DATA = new JSONObject();
                DATA.put("x-param", "abc");
                DATA.put("y-param", "def");
                DATA.put("z-param", "ghi");
        }

        @Test
        public void testSubjectSessionToJSONObject_minimal() {

                var session = new SubjectSession() {
                        @Override
                        public Subject getSubject() {
                                return SUBJECT;
                        }

                        @Override
                        public SubjectAuthentication getSubjectAuthentication() {
                                return new SubjectAuthentication() {
                                        @Override
                                        public Subject getSubject() {
                                                return SUBJECT;
                                        }

                                        @Override
                                        public Instant getTime() {
                                                return ONE_HOUR_AGO;
                                        }

                                        @Override
                                        public @Nullable ACR getACR() {
                                                return null;
                                        }

                                        @Override
                                        public @Nullable List<AMR> getAMRList() {
                                                return null;
                                        }
                                };
                        }

                        @Override
                        public Instant getCreationTime() {
                                return ONE_DAY_AGO;
                        }

                        @Override
                        public long getMaxLifetime() {
                                return 60 * 24 * 30;
                        }

                        @Override
                        public long getAuthLifetime() {
                                return 60 * 24 * 2;
                        }

                        @Override
                        public long getMaxIdleTime() {
                                return 60 * 24;
                        }

                        @Override
                        public Set<ClientID> getRelyingParties() {
                                return RELYING_PARTIES;
                        }

                        @Override
                        public JSONObject getClaims() {
                                return null;
                        }

                        @Override
                        public JSONObject getData() {
                                return null;
                        }
                };

                JSONObject o = JSONUtils.toJSONObject(session);

                assertEquals(SUBJECT.getValue(), o.get("sub"));
                assertEquals(ONE_HOUR_AGO.getEpochSecond(), o.get("auth_time"));
                assertEquals(ONE_DAY_AGO.getEpochSecond(), o.get("creation_time"));
                assertEquals(60 * 24 * 30L, o.get("max_life"));
                assertEquals(60 * 24 * 2L, o.get("auth_life"));
                assertEquals(60 * 24L, o.get("max_idle"));
                assertEquals(Identifier.toStringList(RELYING_PARTIES), o.get("rps"));
                assertEquals(7, o.size());
        }

        @Test
        public void testSubjectSessionToJSONObject_fullySpecified() {

                var session = new SubjectSession() {
                        @Override
                        public Subject getSubject() {
                                return SUBJECT;
                        }

                        @Override
                        public SubjectAuthentication getSubjectAuthentication() {
                                return new SubjectAuthentication() {
                                        @Override
                                        public Subject getSubject() {
                                                return SUBJECT;
                                        }

                                        @Override
                                        public Instant getTime() {
                                                return ONE_HOUR_AGO;
                                        }

                                        @Override
                                        public ACR getACR() {
                                                return ACR;
                                        }

                                        @Override
                                        public @Nullable List<AMR> getAMRList() {
                                                return AMR_LIST;
                                        }
                                };
                        }

                        @Override
                        public Instant getCreationTime() {
                                return ONE_DAY_AGO;
                        }

                        @Override
                        public long getMaxLifetime() {
                                return 60 * 24 * 30;
                        }

                        @Override
                        public long getAuthLifetime() {
                                return 60 * 24 * 2;
                        }

                        @Override
                        public long getMaxIdleTime() {
                                return 60 * 24;
                        }

                        @Override
                        public Set<ClientID> getRelyingParties() {
                                return RELYING_PARTIES;
                        }

                        @Override
                        public @Nullable JSONObject getClaims() {
                                return CLAIMS;
                        }

                        @Override
                        public @Nullable JSONObject getData() {
                                return DATA;
                        }
                };

                JSONObject o = JSONUtils.toJSONObject(session);

                assertEquals(SUBJECT.getValue(), o.get("sub"));
                assertEquals(ONE_HOUR_AGO.getEpochSecond(), o.get("auth_time"));
                assertEquals(ACR.getValue(), o.get("acr"));
                assertEquals(Identifier.toStringList(AMR_LIST), o.get("amr"));
                assertEquals(ONE_DAY_AGO.getEpochSecond(), o.get("creation_time"));
                assertEquals(60 * 24 * 30L, o.get("max_life"));
                assertEquals(60 * 24 * 2L, o.get("auth_life"));
                assertEquals(60 * 24L, o.get("max_idle"));
                assertEquals(Identifier.toStringList(RELYING_PARTIES), o.get("rps"));
                assertEquals(CLAIMS, o.get("claims"));
                assertEquals(DATA, o.get("data"));
                assertEquals(11, o.size());
        }
}
