package com.nimbusds.openid.connect.provider.spi.claims.http;


import org.junit.Test;

import static org.junit.Assert.assertEquals;


public class LoggersTest {
	

	@Test
	public void testLoggerNames() {
		
		assertEquals("MAIN", Loggers.MAIN_LOG.getName());
		assertEquals("USERINFO", Loggers.USERINFO_LOG.getName());
	}
}
