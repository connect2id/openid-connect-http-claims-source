package com.nimbusds.openid.connect.provider.spi.claims.http;


import org.junit.Test;

import java.util.Collections;
import java.util.Properties;

import static org.junit.Assert.*;


public class ConfigurationTest {


	@Test
	public void testParse() {

		var props = new Properties();

		props.setProperty("op.httpClaimsSource.enable", "true");
		props.setProperty("op.httpClaimsSource.supportedClaims", "email, email_verified, name, given_name, family_name");
		props.setProperty("op.httpClaimsSource.url", "https://example.com/claims-source");
		props.setProperty("op.httpClaimsSource.connectTimeout", "250");
		props.setProperty("op.httpClaimsSource.readTimeout", "500");
		props.setProperty("op.httpClaimsSource.trustSelfSignedCerts", "false");
		props.setProperty("op.httpClaimsSource.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertTrue(config.supportedClaims.contains("email"));
		assertTrue(config.supportedClaims.contains("email_verified"));
		assertTrue(config.supportedClaims.contains("name"));
		assertTrue(config.supportedClaims.contains("given_name"));
		assertTrue(config.supportedClaims.contains("family_name"));
		assertEquals(5, config.supportedClaims.size());
		assertEquals("https://example.com/claims-source", config.url.toString());
		assertEquals(250, config.connectTimeout);
		assertEquals(500, config.readTimeout);
		assertFalse(config.trustSelfSignedCerts);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.apiAccessToken.getValue());
		assertNull(config.apiAccessToken.getScope());
		assertFalse(config.includeSubjectSessionID);
		assertFalse(config.includeSubjectSession);
		assertFalse(config.includeScope);

		config.log();
	}
	
	
	@Test
	public void testParse_wildCardForCatchCallConfig() {

		var props = new Properties();

		props.setProperty("op.httpClaimsSource.enable", "true");
		props.setProperty("op.httpClaimsSource.supportedClaims", "*");
		props.setProperty("op.httpClaimsSource.url", "https://example.com/claims-source");
		props.setProperty("op.httpClaimsSource.connectTimeout", "0");
		props.setProperty("op.httpClaimsSource.readTimeout", "0");
		props.setProperty("op.httpClaimsSource.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");

		Configuration config = new Configuration(props);

		assertTrue(config.enable);
		assertEquals(Collections.singleton("*"), config.supportedClaims);
		assertEquals("https://example.com/claims-source", config.url.toString());
		assertEquals(0, config.connectTimeout);
		assertEquals(0, config.readTimeout);
		assertFalse(config.trustSelfSignedCerts);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.apiAccessToken.getValue());
		assertNull(config.apiAccessToken.getScope());
		assertFalse(config.includeSubjectSessionID);
		assertFalse(config.includeSubjectSession);
		assertFalse(config.includeScope);

		config.log();
	}


	@Test
	public void testParse_includeSubjectSessionID() {

		var props = new Properties();

		props.setProperty("op.httpClaimsSource.enable", "true");
		props.setProperty("op.httpClaimsSource.supportedClaims", "email,email_verified");
		props.setProperty("op.httpClaimsSource.url", "https://example.com/claims-source");
		props.setProperty("op.httpClaimsSource.connectTimeout", "1000");
		props.setProperty("op.httpClaimsSource.readTimeout", "3000");
		props.setProperty("op.httpClaimsSource.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");
		props.setProperty("op.httpClaimsSource.includeSubjectSessionID", "true");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertTrue(config.supportedClaims.contains("email"));
		assertTrue(config.supportedClaims.contains("email_verified"));
		assertEquals(2, config.supportedClaims.size());
		assertEquals("https://example.com/claims-source", config.url.toString());
		assertEquals(1000, config.connectTimeout);
		assertEquals(3000, config.readTimeout);
		assertFalse(config.trustSelfSignedCerts);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.apiAccessToken.getValue());
		assertNull(config.apiAccessToken.getScope());
		assertTrue(config.includeSubjectSessionID);
		assertFalse(config.includeSubjectSession);
		assertFalse(config.includeScope);

		config.log();
	}


	@Test
	public void testParse_includeSubjectSession() {

		var props = new Properties();

		props.setProperty("op.httpClaimsSource.enable", "true");
		props.setProperty("op.httpClaimsSource.supportedClaims", "email,email_verified");
		props.setProperty("op.httpClaimsSource.url", "https://example.com/claims-source");
		props.setProperty("op.httpClaimsSource.connectTimeout", "1000");
		props.setProperty("op.httpClaimsSource.readTimeout", "3000");
		props.setProperty("op.httpClaimsSource.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");
		props.setProperty("op.httpClaimsSource.includeSubjectSession", "true");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertTrue(config.supportedClaims.contains("email"));
		assertTrue(config.supportedClaims.contains("email_verified"));
		assertEquals(2, config.supportedClaims.size());
		assertEquals("https://example.com/claims-source", config.url.toString());
		assertEquals(1000, config.connectTimeout);
		assertEquals(3000, config.readTimeout);
		assertFalse(config.trustSelfSignedCerts);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.apiAccessToken.getValue());
		assertNull(config.apiAccessToken.getScope());
		assertFalse(config.includeSubjectSessionID);
		assertTrue(config.includeSubjectSession);
		assertFalse(config.includeScope);

		config.log();
	}


	@Test
	public void testParse_includeScope() {

		var props = new Properties();

		props.setProperty("op.httpClaimsSource.enable", "true");
		props.setProperty("op.httpClaimsSource.supportedClaims", "email,email_verified");
		props.setProperty("op.httpClaimsSource.url", "https://example.com/claims-source");
		props.setProperty("op.httpClaimsSource.connectTimeout", "1000");
		props.setProperty("op.httpClaimsSource.readTimeout", "3000");
		props.setProperty("op.httpClaimsSource.apiAccessToken", "ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6");
		props.setProperty("op.httpClaimsSource.includeScope", "true");

		var config = new Configuration(props);

		assertTrue(config.enable);
		assertTrue(config.supportedClaims.contains("email"));
		assertTrue(config.supportedClaims.contains("email_verified"));
		assertEquals(2, config.supportedClaims.size());
		assertEquals("https://example.com/claims-source", config.url.toString());
		assertEquals(1000, config.connectTimeout);
		assertEquals(3000, config.readTimeout);
		assertFalse(config.trustSelfSignedCerts);
		assertEquals("ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6", config.apiAccessToken.getValue());
		assertNull(config.apiAccessToken.getScope());
		assertFalse(config.includeSubjectSessionID);
		assertFalse(config.includeSubjectSession);
		assertTrue(config.includeScope);

		config.log();
	}
	
	
	@Test
	public void testDisabledByDefault() {
		
		var config = new Configuration(new Properties());
		assertFalse(config.enable);
		assertTrue(config.supportedClaims.isEmpty());
		assertNull(config.url);
		assertEquals(0, config.connectTimeout);
		assertEquals(0, config.readTimeout);
		assertFalse(config.trustSelfSignedCerts);
		assertNull(config.apiAccessToken);
		assertFalse(config.includeSubjectSessionID);
		assertFalse(config.includeSubjectSession);
		assertFalse(config.includeScope);

		config.log();
	}
}
