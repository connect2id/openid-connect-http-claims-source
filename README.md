# Simple HTTP Based Claims Source

Copyright (c) Connect2idLtd., 2016 - 2024

Connector for sourcing OpenID Connect claims about a subject (end-user) from an
HTTP endpoint. Implements the
[AdvancedClaimsSource](https://www.javadoc.io/doc/com.nimbusds/c2id-server-sdk/latest/com/nimbusds/openid/connect/provider/spi/claims/AdvancedClaimsSource.html)
SPI from the Connect2id server SDK.

* Supports retrieval of arbitrary OpenID Connect claims.

* Supports multiple scripts and languages via language tags.

* Access to the HTTP endpoint requires a non-expiring bearer token.

* Utilises an HTTP POST request to obtain the claims in order to prevent
  leaking of the request parameters (subject identifier and claim names) into
  HTTP server logs.



## Configuration

The HTTP claims source is configured from Java system properties or from a
properties file `/WEB-INF/httpClaimsSource.properties`. The Java system
properties have precedence. If no configuration properties are found the HTTP
claims source is disabled.

* op.httpClaimsSource.enable -- Enables / disables the HTTP claims source.
  Disabled by default.
* op.httpClaimsSource.supportedClaims -- The names of the supported (standard
  and custom) OpenID Connect claims, as a comma and / or space separated list.
  Support for a pattern of claims can be indicated with the `*` wildcard 
  character, for example as `https://idp.example.com/*` for a set of claims 
  having a common URI prefix in their name. A single claim set to `*` indicates 
  support for all claims supported by the OpenID provider without explicitly 
  listing them.
* op.httpClaimsSource.url -- The URL of the HTTP endpoint for sourcing the
  OpenID Connect claims. Should be HTTPS.
* op.httpClaimsSource.connectTimeout -- The timeout in milliseconds for
  establishing HTTP connections. If zero the underlying HTTP client library
  will determine the timeout.
* op.httpClaimsSource.readTimeout -- The timeout in milliseconds for obtaining
  HTTP responses after connection. If zero the underlying HTTP client library
  will determine the timeout.
* op.httpClaimsSource.trustSelfSignedCerts -- Determines whether to accept
  self-signed X.509 / TLS certificates presented by the HTTP server.
  Self-signed certificates are not trusted by default.
* op.httpClaimsSource.apiAccessToken -- Access token of type bearer
  (non-expiring) for accessing the HTTP endpoint. Should contain at least 32
  random alphanumeric characters to make brute force guessing impractical.
* op.httpClaimsSource.includeSubjectSessionID -- Enables / disables inclusion 
  of the subject (end-user) session ID in the request where the claims sourcing 
  was authorised. Disabled by default.
* op.httpClaimsSource.includeSubjectSession -- Enables / disables inclusion of
  the subject (end-user) session in the request where the claims sourcing was 
  authorised. Disabled by default.
* op.httpClaimsSource.includeScope -- Enables / disables inclusion of the 
  associated consented scope. Disabled by default.

Example configuration properties:

```ini
op.httpClaimsSource.enable=true
op.httpClaimsSource.supportedClaims=email,email_verified,name,given_name,family_name
op.httpClaimsSource.url=https://example.com/claims-source
op.httpClaimsSource.connectTimeout=1000
op.httpClaimsSource.readTimeout=1000
op.httpClaimsSource.apiAccessToken=ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6
```

Example configuration for a source that will handle all claims supported by the
OpenID provider without explicitly listing them:

```ini
op.httpClaimsSource.enable=true
op.httpClaimsSource.supportedClaims=*
op.httpClaimsSource.url=https://example.com/claims-source
op.httpClaimsSource.connectTimeout=1000
op.httpClaimsSource.readTimeout=1000
op.httpClaimsSource.apiAccessToken=ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6
```


## Web API

The claims request is authorised with a Bearer token and POSTed as a JSON
object with members:

* `iss` {string} -- The OpenID provider issuer URL.
* `sub` {string} -- The subject (end-user) for which OpenID claims are requested.
* `claims` {string array} -- The names of the requested OpenID claims, with
  optional language tags.
* `claims_data` {object} -- Optional data to aid the claims provision, omitted 
  if none (maps from `ClaimsSourceRequestContext.getClaimsData`).
* `claims_transport` {string} -- Hints how the requested claims are going to be 
  transported to the relying party: "userinfo" for the UserInfo endpoint, 
  "id_token" for the ID token, omitted if not applicable (for claims requested
  by an internal plugin, such as an access token codec).
* `sub_sid` {string} -- Optional subject (end-user) session ID. Included when 
  the `op.httpClaimsSource.includeSubjectSessionID` configuration property is
  enabled and the subject session where the claims sourcing was authorised is 
  still active (not closed or expired).
* `sub_session` {object} -- Optional subject (end-user) session details. The 
  JSON object structure is identical to the subject session object of the 
  Connect2id server subject session store web API. Included when the 
  `op.httpClaimsSource.includeSubjectSession` configuration property is enabled
  and the subject session where the claims sourcing was authorised is still 
  active (not closed or expired).
* `scope` {string array} -- Optional associated consented scope. Included when
  the `op.httpClaimsSource.includeScope` configuration property is enabled and
  the scope is available.

Example claims request to the HTTP endpoint:

```http
POST /claims-source HTTP/1.1
Host: www.example.com
Content-Type: application/json; charset=UTF-8
Authorization: Bearer ztucZS1ZyFKgh0tUEruUtiSTXhnexmd6

{
  "iss"              : "https://c2id.com",
  "sub"              : "alice",
  "claims"           : [ "email", "email_verified", "name", "given_name", "family_name" ],
  "claims_transport" : "userinfo"
}
```

The claims must be returned in UserInfo response format, specifying the subject
(`sub`) and all requested claims that can be fulfilled. If a particular claim 
is not available it must be omitted from the response.

Example response:

```http
HTTP/1.1 200 OK
Date: Mon, 23 May 2016 22:38:34 GMT
Content-Type: application/json; charset=UTF-8

{
  "sub"            : "alice",
  "email"          : "alice@wonderland.net",
  "email_verified" : true,
  "name"           : "Alice Adams",
  "given_name"     : "Alice",
  "family_name"    : "Adams"
}
```

If the subject is not found or invalid (e.g. end-user account deleted) the 
response must be an empty JSON object.

```http
HTTP/1.1 200 OK
Date: Mon, 23 May 2016 22:38:34 GMT
Content-Type: application/json; charset=UTF-8

{}
```


## Change log

* 1.0 (2018-05-09)
    * First official release.

* 1.0.1 (2018-06-01)
    * Fixes SPI manifest.
    
* 2.0 (2020-03-17)
    * Adds optional claims_data request parameter, maps from 
      ClaimsSourceRequestContext.getClaimsData.
    * Empty JSON object in response signifies subject invalid or not found.
    * Removes redundant check for supported claims, done by the Connect2id 
      server.
    * Bumps dependencies.
* 2.1 (2020-09-24)
    * Includes a "claims_transport" request parameter hinting how the requested
      claims are going to be transported to the relying party.
* 2.2 (2021-10-27)
    * Includes an "Accept: application/json" HTTP request header (iss #1). 
* 2.2.1 (2021-12-09)
    * Updates the op.httpClaimsSource.supportedClaims documentation to 
      describe "*" wildcard use.  
* 2.3 (2024-01-24)
    * Upgrades to Java 11. 
    * Upgrades to Connect2id server v14.0 API / server SDK 4.58.
    * Adds an optional op.httpClaimsSource.includeSubjectSession configuration 
      property that determines whether to expose the optional SubjectSession 
      returned by ClaimsSourceRequestContext.getSubjectSession as a 
      "sub_session" member using the subject session JSON format established by
      the Connect2id server subject session store web API.
    * Bumps minimal dependencies.
* 3.0 (2024-02-03)
    * Upgrades to Java 17.
    * Upgrades to Connect2id server v15.2 API / server SDK 5.2.
    * Adds an optional op.httpClaimsSource.includeSubjectSessionID 
      configuration property that determines whether to expose the optional 
      SubjectSessionID (SID) returned by ClaimsSourceRequestContext.
      getSubjectSessionID as a "sub_sid" member of type JSON string.
    * Bumps minimal dependencies.
* 3.1 (2024-04-23)
    * Adds an optional op.httpClaimsSource.includeScope configuration property 
      that determines whether to expose the optional associated consented scope 
      returned by ClaimsSourceRequestContext.getScope as a "scope" member of 
      type JSON array.
    * Bumps dependencies.   

Questions or comments? Email support@connect2id.com
